import * as React from 'react';
import * as Localization from 'expo-localization';
import * as Font from 'expo-font';
import { Platform, StatusBar, StyleSheet, View, Text } from 'react-native';
import * as SplashScreen from 'expo-splash-screen';
import { NavigationContainer } from '@react-navigation/native';
import useLinking from './app/navigation/useLinking';
import databaseService from "./app/services/databaseService";
import StackNavigation from "./app/navigation/StackNavigation";
import { MenuProvider } from 'react-native-popup-menu';
import i18n from 'i18n-js';
import { LocalizationContext } from "./LocalizationContext";
import Colors from "./app/constants/Colors";
import { LogBox } from 'react-native';
import * as Sentry from 'sentry-expo';
import { ToastProvider } from "react-native-fast-toast";

Sentry.init({
  dsn: 'https://27c0923366f64891aa8da8ae7c10f4e3@o513983.ingest.sentry.io/5616641',
  enableInExpoDevelopment: true,
  debug: true, // Sentry will try to print out useful debugging information if something goes wrong with sending an event. Set this to `false` in production.
});

export default function App(props) {
    const [isLoadingComplete, setLoadingComplete] = React.useState(false);
    const [initialNavigationState, setInitialNavigationState] = React.useState();
    const containerRef = React.useRef();
    const { getInitialState } = useLinking(containerRef);

    const [locale, setLocale] = React.useState(Localization.locale);
    const localizationContext = React.useMemo(
        () => ({
            t: (scope, options) => i18n.t(scope, { locale, ...options }),
            locale,
            setLocale,
        }),
        [locale]
        );

    const MyTheme = {
        colors: {
            primary: Colors.headerTabColor,
            background: Colors.screenBackground
        },
    };

    function initAirtable(){
        let Airtable = require('airtable');
        Airtable.configure({
            endpointUrl: 'https://api.airtable.com',
            apiKey: 'keyLPUoojvySEswR0'
        });
        LogBox.ignoreLogs(['Setting a timer']);
    }
    // Load any resources or data that we need prior to rendering the app
    React.useEffect(() => {
        try {
            Font.loadAsync({
                'nunito-regular': require('./assets/fonts/nunito.regular.ttf'),
                'nunito-semibold': require('./assets/fonts/nunito.semibold.ttf'),
                'nunito-bold': require('./assets/fonts/nunito.bold.ttf'),
                'nunito-extrabold': require('./assets/fonts/nunito.extrabold.ttf'),
                'noto-sans-ui': require('./assets/fonts/NotoSansUI-Regular.ttf')
            })
        } catch (e) {
            Sentry.captureException(e);
        }

        Text.defaultProps = Text.defaultProps || {};
        // Ignore dynamic type scaling
        Text.defaultProps.allowFontScaling = false;

        async function loadResourcesAndDataAsync() {
            try {
                await SplashScreen.preventAutoHideAsync();
            } catch (e) {
                //do nothing since the error has been benign
            }

            try {
                // Load our initial navigation state
                setInitialNavigationState(await getInitialState());

                await databaseService.copyDatabase();
                setLoadingComplete(true);
            } catch (e) {
                Sentry.captureException(new Error('Could not properly loadResourcesAndDataAsync'));
                setLoadingComplete(false);
            } finally {
                await SplashScreen.hideAsync();
            }
        }

        loadResourcesAndDataAsync();
        initAirtable();
    }, []);

    if (!isLoadingComplete && !props.skipLoadingScreen) {
        return null;
    } else {
        return (
            <ToastProvider>
                <View style={styles.topContainer}>
                    <MenuProvider>
                        <View style={styles.mainContainer}>
                            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                            <NavigationContainer initialState={initialNavigationState}>
                                <LocalizationContext.Provider value={localizationContext}>
                                    <StackNavigation />
                                </LocalizationContext.Provider>
                            </NavigationContainer>
                        </View>
                    </MenuProvider>
                </View>
            </ToastProvider>
        );
    }
}

const styles = StyleSheet.create({
    topContainer: {
        flex: 1,
        backgroundColor: Colors.defaultBackground
    },
    mainContainer: {
        flex: 1
    },
});
