Prerequirements :
- node version 14.0.0
- npm

Install Expo CLI :
- https://docs.expo.io/workflow/expo-cli/

How to launch the app :
In a terminal launch :
- yarn
- expo start

You can run the app on Android or iOS emulator
For Android : Install Android Studio to emulate a device
For iOS : Install XCode to simulate a device

Or you can also install the Expo App in your phone, and you can scan the QR Code of the launching page when you run 'expo start' to run the app in your smartphone

Publish the app for demo and test :
- run 'expo publish', it will publish on expo server : https://expo.io/@nonodarko/projects/whattcsayv3
To test the app, you have to scan the QR Code on the url above with Expo app on your phone

To build the app :
- Android : run expo build:android, it will generate an apk
- iOS : run expo build:ios, it will generate an archive file

To build with Sentry integration:
- Check out the Expo guide for [Sentry](https://docs.expo.io/guides/using-sentry/) for the basic coverage
- For source code mapping, run `expo publish`
- To throw Sentry exceptions, add this bit of code `Sentry.captureException(new Error('Fonts did not load!'))`
- If you need access to Sentry, ask one of the repo admins.

To test the release version:
- Start expo servers with `expo start --no-dev --minify`