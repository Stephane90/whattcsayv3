import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Colors from "../constants/Colors";
import {LocalizationContext} from "../../LocalizationContext";
import * as Dialect from "../constants/Dialect";
import * as Origin from "../constants/Origin";

const EtymologyText = ({dialect, origin}) => {

    const { t } = React.useContext(LocalizationContext);
    const translatedDialect = translate(dialect, Dialect.map);
    const translatedOrigin = translate(origin, Origin.map);

    function translate(entry, map) {
        const entries = entry.split(', ');
        let translated = '';
        entries.forEach(entry =>
            translated = translated + t(map.get(entry)) + ', '
        );

        return translated.slice(0, -2);
    }

    function formatEtymology(){
        let etymology;
        if (dialect != '' && origin != '') {
            etymology = '('+ translatedDialect + ', ' + translatedOrigin + ')';
        } else if(dialect == '' && origin != ''){
            etymology = '(' + translatedOrigin + ')';
        } else if(dialect != '' && origin == ''){
            etymology = '(' + translatedDialect + ')';
        } else {
            etymology = '';
        }
        return etymology;
    }

    return (
        <View style={styles.container}>
            <Text style={styles.text}>{formatEtymology()}</Text>
        </View>
    );
}

export default EtymologyText

const styles = StyleSheet.create({
    container : {
        paddingLeft: 42,
    },
    text: {
        fontFamily: 'nunito-regular',
        fontWeight: '400',
        fontSize: 12,
        color: Colors.lightGrey
    }
})