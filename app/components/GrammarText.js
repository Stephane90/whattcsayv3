import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Colors from "../constants/Colors";
import * as PartOfSpeech from "../constants/PartOfSpeech";
import * as Register from "../constants/Register";
import {LocalizationContext} from "../../LocalizationContext";

const GrammarText = ({partOfSpeech, register}) => {

    const { t } = React.useContext(LocalizationContext);
    const translatedPartOfSpeech = translate(partOfSpeech, PartOfSpeech.map);
    const translatedRegister = translate(register, Register.map);

    function translate(entry, map) {
        const entries = entry.split(', ');
        let translated = '';
        entries.forEach(entry =>
            translated = translated + t(map.get(entry)) + ', '
        );

        return translated.slice(0, -2);
    }

    function formatGrammar(){
        let grammar;
        if (partOfSpeech != '' && register != '') {
            grammar = translatedPartOfSpeech + ', ' + translatedRegister;
        } else if(partOfSpeech == '' && register != ''){
            grammar = translatedRegister;
        } else if(partOfSpeech != '' && register == ''){
            grammar = translatedPartOfSpeech;
        } else {
            grammar = '';
        }
        return grammar;
    }

    return (
        <View style={styles.container}>
            <Text style={styles.text}>{formatGrammar()}</Text>
        </View>
    );
}

export default GrammarText

const styles = StyleSheet.create({
    container : {
        paddingLeft: 16
    },
    text: {
        fontFamily: 'nunito-regular',
        fontWeight: '400',
        fontSize: 12,
        color: Colors.lightGrey
    }
})