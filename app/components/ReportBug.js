import React from "react";
import {Platform, Button, Dimensions, Modal, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import Colors from "../constants/Colors";
import Airtable from "airtable";
import {LocalizationContext} from "../../LocalizationContext";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { useToast } from 'react-native-fast-toast'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const ReportBug = ({item}) => {

    const toast = useToast()
    const {t, locale} = React.useContext(LocalizationContext);
    const [typeBug, setTypeBug] = React.useState("")
    const [definitionValue, setDefinitionValue] = React.useState(item.def);
    const [chineseValue, setChineseValue] = React.useState(item.chinese_char);
    const [pengimValue, setPengimValue] = React.useState(item.romanization);
    const [commentValue, setCommentValue] = React.useState("");
    const [partOfSpeechValue, setPartOfSpeechValue] = React.useState(item.part_of_speech);
    const [registerValue, setRegisterValue] = React.useState(item.register);
    const [dialectValue, setDialectValue] = React.useState(item.dialect);
    const [originValue, setOriginValue] = React.useState(item.origin);
    const [isModalVisible, setModalVisible] = React.useState(false)

    // Create toggleModalVisibility function that will open and close modal upon button clicks.
    const toggleModalVisibility = () => {
        setModalVisible(!isModalVisible);
    };

    function sendReport(item) {
        let base = Airtable.base('apps7NknXxmSXT5B5');
        base('Bug report').create([
            {
                "fields": {
                    "Id": item.definition_id,
                    "def": definitionValue,
                    "romanization": pengimValue,
                    "chinese": chineseValue,
                    "locale": locale,
                    "part of speech": partOfSpeechValue,
                    "register": registerValue,
                    "dialect": dialectValue,
                    "origin": originValue,
                    "typeBug": typeBug,
                    "comment": commentValue
                }
            }
        ], function (err, records) {
            if (err) {
                console.error(err);
                return;
            }
        });
        setModalVisible(!isModalVisible);
        toast.show( t('reported'), { type: "success" });
    }

    return (
        <TouchableOpacity style={{padding: 5}} onPress={() => toggleModalVisibility()}>
            <MaterialCommunityIcons
                name='alert-circle'
                size={15}
                color={Colors.tabIconDefault}
            />
            <Modal animationType="fade"
                   transparent visible={isModalVisible}
                   presentationStyle="overFullScreen"
                   onDismiss={() => toggleModalVisibility}>
                <View style={styles.viewWrapper}>
                    <View style={styles.modalView}>
                        <View><Text>{t('reportPopinTitle')}</Text></View>
                        <View style={{flex: 1, width: "100%", paddingVertical: 20}}>

                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('definition')}</Text>
                                <TextInput placeholder={item.def}
                                           value={definitionValue} style={styles.textInput}
                                           onChangeText={(value) => setDefinitionValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('romanization')}</Text>
                                <TextInput placeholder={item.romanization}
                                           value={pengimValue} style={styles.textInput}
                                           onChangeText={(value) => setPengimValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('chinese_char')}</Text>
                                <TextInput placeholder={item.chinese_char}
                                           value={chineseValue} style={styles.textInput}
                                           onChangeText={(value) => setChineseValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('partOfSpeech')}</Text>
                                <TextInput placeholder={item.part_of_speech}
                                           value={partOfSpeechValue} style={styles.textInput}
                                           onChangeText={(value) => setPartOfSpeechValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('register')}</Text>
                                <TextInput placeholder={item.register}
                                           value={registerValue} style={styles.textInput}
                                           onChangeText={(value) => setRegisterValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('dialect')}</Text>
                                <TextInput placeholder={item.dialect}
                                           value={dialectValue} style={styles.textInput}
                                           onChangeText={(value) => setDialectValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('origin')}</Text>
                                <TextInput placeholder={item.origin}
                                           value={originValue} style={styles.textInput}
                                           onChangeText={(value) => setOriginValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('comment')}</Text>
                                <TextInput placeholder={t('comment')}
                                           value={commentValue} style={styles.textInput}
                                           onChangeText={(value) => setCommentValue(value)}/>
                            </View>
                            <View style={styles.rowView}>
                                <Text style={styles.rowLabel}>{t('typeOfBug')}</Text>
                                <View style={
                                    (Platform.OS !== 'android' && { zIndex: 10})
                                }><DropDownPicker
                                    items={[
                                        {label: t('definition'), value: 'definition'},
                                        {label: t('romanization'), value: 'pengim'},
                                        {label: t('chinese_char'), value: 'chinese'},
                                        {label: t('partOfSpeech'), value: 'partOfSpeech'},
                                        {label: 'Register', value: 'register'},
                                        {label: t('dialect'), value: 'dialect'},
                                        {label: t('origin'), value: 'origin'}
                                    ]}
                                    style={{backgroundColor: '#fafafa'}}
                                    defaultValue={typeBug}
                                    containerStyle={styles.dropDownContainer}
                                    labelStyle={{
                                        fontSize: 12,
                                        textAlign: 'right',
                                    }}
                                    itemStyle={{
                                        justifyContent: 'flex-end',
                                        fontSize: 6
                                    }}
                                    dropDownStyle={{backgroundColor: '#fafafa'}}
                                    onChangeItem={picker => setTypeBug(picker.value)}
                                />
                                </View>
                            </View>
                        </View>
                        <View style={{flexDirection: 'row', paddingTop: 40, zIndex: 1}}>
                            <View style={{paddingHorizontal: 10}}>
                                <Button title={t('cancelButton')} color={Colors.silverColor} onPress={toggleModalVisibility}/>
                            </View>
                            <View style={{paddingHorizontal: 10}}>
                                <Button title={t('reportButton')} color={Colors.headerTabColor} onPress={() => sendReport(item)}/>
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        </TouchableOpacity>
    );
}

export default ReportBug

const styles = StyleSheet.create({
    viewWrapper: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0, 0, 0, 0.2)",
    },
    modalView: {
        padding: 15,
        alignItems: "center",
        justifyContent: "center",
        position: "absolute",
        top: "20%",
        left: "50%",
        elevation: 5,
        transform: [
            {translateX: -(width * 0.45)},
            {translateY: -(height * 0.15)}],
        height: height * 0.9,
        width: width * 0.9,
        backgroundColor: "#fff",
        borderRadius: 5,
    },
    dropDownContainer: {
        height: 30,
        width: 140
    },
    rowLabel: {
        borderColor: Colors.silverColor,
        fontSize: 12,
        justifyContent: 'space-evenly'
    },
    textInput: {
        borderRadius: 5,
        paddingHorizontal: 5,
        paddingVertical: 3,
        borderColor: Colors.silverColor,
        borderWidth: 1,
        fontSize: 12,
        width: 140,
        justifyContent: 'space-evenly'
    },
    rowView: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5,
        justifyContent: 'space-between'
    }
});

