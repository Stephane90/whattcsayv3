export const map = new Map();

map.set('Teochew', 'Teochew');
map.set('Overseas Teochew', 'Overseas_Teochew');
map.set('Hokkien', 'Hokkien');
map.set('Malay-TC', 'Malay_TC');
map.set('Mandarin', 'Mandarin');
map.set('Cantonese', 'Cantonese');
map.set('Hakka', 'Hakka');
map.set('Malay', 'Malay');
map.set('Thai', 'Thai');
map.set('Cambodian', 'Cambodian');
map.set('Vietnamese', 'Vietnamese');
map.set('Singaporean', 'Singaporean');
map.set('Indonesian', 'Indonesian');
map.set('Japanese', 'Japanese');
map.set('English', 'English');
map.set('French', 'French');
