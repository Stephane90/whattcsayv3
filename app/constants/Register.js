export const map = new Map();

map.set('vulgar', 'vulgar');
map.set('colloquial', 'colloquial');
map.set('polite', 'polite');
map.set('literary', 'literary');
map.set('formal', 'formal');
map.set('dated', 'dated');
