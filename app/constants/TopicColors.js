const topicColor1 = '#2C96F9';
const topicColor2 = '#2C96F9';
const topicColor3 = '#F9D22A';
const topicColor4 = '#A478FB';
const topicColor5 = '#FC5752';
const topicColor6 = '#EF5E9B';
const topicColor7 = '#2ECB7C';
const topicColor8 = '#14BDEF';
const topicColor9 = '#F77744';
const topicColor10 = '#8B2D91';
const topicColor11 = '#8FCB2E';
const topicColor12 = '#64C1C1';
const topicColor13 = '#FF9E9E';
const topicColor14 = '#E2A813';

export const topicMap = new Map();

topicMap.set(1, topicColor1);
topicMap.set(2, topicColor2);
topicMap.set(3, topicColor3);
topicMap.set(4, topicColor4);
topicMap.set(5, topicColor5);
topicMap.set(6, topicColor6);
topicMap.set(7, topicColor7);
topicMap.set(8, topicColor8);
topicMap.set(9, topicColor9);
topicMap.set(10, topicColor10);
topicMap.set(11, topicColor11);
topicMap.set(12, topicColor12);
topicMap.set(13, topicColor13);
topicMap.set(14, topicColor14);

class TopicColors {

    getTopicColorById(topicId) {
        return topicMap.get(topicId);
    }
}

const topicColors = new TopicColors();
export default topicColors;