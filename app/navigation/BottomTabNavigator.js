import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';


import * as React from 'react';
import Colors from '../constants/Colors';

import TabBarIcon from './TabBarIcon';
import SearchScreen from '../screens/SearchScreen';
import TopicsScreen from '../screens/TopicsScreen';
import OptionMenu from "./OptionMenu";
import { LocalizationContext } from "../../LocalizationContext";
import {Platform, View} from "react-native";


const BottomTab = createBottomTabNavigator();
const Tab = createMaterialTopTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';

const BottomTabNavigator = ({ navigation, route }) => {

    const { t } = React.useContext(LocalizationContext);

    React.useEffect(() => {
        navigation.setOptions(
            {
                headerTitle: getHeaderTitle(route),
                headerStyle: {
                    backgroundColor: Colors.headerTabColor,
                    height : 100,
                    borderBottomLeftRadius: 50,
                    borderBottomRightRadius: 50,
                },
                headerRightContainerStyle : {
                    paddingRight: Platform.OS === 'ios' ? 5 : 0
                },
                headerTintColor: Colors.white,
                headerTitleStyle: {
                    fontSize : 20,
                    alignSelf: 'center',
                    fontFamily: 'nunito-extrabold',
                },
                headerRight: () => (
                    <OptionMenu />
                ),
                headerLeft: () => (
                    <View />
                )
            });
    });

    function getHeaderTitle(route) {
        const routeName = getFocusedRouteNameFromRoute(route) ?? INITIAL_ROUTE_NAME;

        switch (routeName) {
            case 'Home':
                return t('search');
            case 'Topics':
                return t('topics');
        }
    }

    return (
        <Tab.Navigator tabBarPosition={'bottom'}
                       tabBarOptions={{
                           iconStyle : {height: 35, width: 35},
                           indicatorStyle: { borderWidth: 2, borderColor: Colors.headerTabColor},
                           showIcon: true,
                           activeTintColor: Colors.headerTabColor
                       }}>
            <Tab.Screen name="Home" component={SearchScreen}
                        options={({ navigation, route }) => ({
                            title: t('search'),
                            tabBarLabel:() => {return null},
                            tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="magnify"/>,
                            inactiveBackgroundColor: Colors.screenBackground
                        })}
            />
            <Tab.Screen name="Topics" component={TopicsScreen}
                        options={({ route }) => ({
                            gestureDirection: 'horizontal',
                            title: t('topics'),
                            tabBarLabel:() => {return null},
                            tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="view-grid"/>,
                        })}
            />
        </Tab.Navigator>
    );
}

export default BottomTabNavigator


