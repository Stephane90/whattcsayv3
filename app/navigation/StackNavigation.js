import {createStackNavigator} from "@react-navigation/stack";
import BottomTabNavigator from "./BottomTabNavigator";
import SearchScreen from "../screens/SearchScreen";
import SubTopicList from "../screens/SubTopicList";
import Colors from "../constants/Colors";
import TopicList from "../screens/TopicList";
import WordsScreen from "../screens/WordsScreen";
import * as React from "react";
import {CardStyleInterpolators} from "react-navigation-stack";
import {LocalizationContext} from "../../LocalizationContext";
import OptionMenu from "./OptionMenu";
import {Platform, Dimensions} from "react-native";

const Stack = createStackNavigator();

const StackNavigation = () => {

    const { t } = React.useContext(LocalizationContext);

    return (
        <Stack.Navigator
            screenOptions={{
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
                headerStyle: {
                    backgroundColor: Colors.headerTabColor,
                    height: 100,
                    borderBottomLeftRadius: 50,
                    borderBottomRightRadius: 50,
                },
                headerBackTitleVisible: false,
                headerLeftContainerStyle : {
                    paddingLeft: Platform.OS === 'ios' ? 5 : 0
                },
                headerRightContainerStyle : {
                    paddingRight: Platform.OS === 'ios' ? 5 : 0
                },
                headerTintColor: Colors.white,
                headerTitleStyle: {
                    width : Dimensions.get('window').width - 100,
                    alignSelf: 'center',
                    fontSize : 20,
                    fontFamily: 'nunito-extrabold'
                },
                headerRight: () => (
                    <OptionMenu />
                )
            }}>
            <Stack.Screen
                name="Root"
                component={BottomTabNavigator} />
            <Stack.Screen
                name="Search"
                component={SearchScreen}
            />
            <Stack.Screen
                name="SubTopics"
                component={SubTopicList}
                options={({ route }) => ({
                    title: route.params.title ?? t('subtopics')
                })}/>
            <Stack.Screen
                name="TopicList"
                component={TopicList}
                options={({ route }) => ({
                    title: t('topics')
                })}/>
            <Stack.Screen
                name="Words"
                component={WordsScreen}
                options={({ route }) => ({
                    title: route.params.title ?? t('words')
                })}/>
        </Stack.Navigator>
    );
}

export default StackNavigation
