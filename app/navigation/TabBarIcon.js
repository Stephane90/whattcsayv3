import { Ionicons } from '@expo/vector-icons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import * as React from 'react';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
    return (
        <MaterialCommunityIcons
            name={props.name}
            size={35}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />
    );
}
