import * as React from 'react';
import { SafeAreaView, StyleSheet, Text, View, TextInput, Image } from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import {useEffect, useState} from "react";
import SearchedWordResult from "../components/SearchedWordResult";
import Colors from "../constants/Colors";
import transactionService from "../services/transactionService";
import { LocalizationContext } from "../../LocalizationContext";

const SearchScreen = ({navigation}) => {

    const { t, locale } = React.useContext(LocalizationContext);
    const [words, setWords] = useState([]);  // For the main data
    const [filteredWords, setFilteredWords] = useState([]); // Filtered data
    const [selectedValue, setSelectedValue] = useState({}); // selected data

    useEffect(() => {
        transactionService.getAutoComplete( locale, results => setWords(results));
    }, []);

    const findWords = (searchInput) => {
        //method called everytime when we change the value of the input
        setTimeout(function(){
            if (searchInput.length >= 1) {
                transactionService.getLikeQuery(searchInput.trim(), locale, results => setFilteredWords(results));
            } else {
                //if the query is null then return blank
                setFilteredWords([]);
            }
        }, 1000);
    };

    return (
        <SafeAreaView style={styles.mainContainer}>
            <View style={styles.autoCompleteContainer}>
                <View style={styles.searchContainer}>
                    <Autocomplete
                        autoCapitalize="none"
                        hideResults={true}
                        renderTextInput={(text) => (
                            <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                <View style={{paddingHorizontal: 8}}>
                                    <Image style={styles.searchBarIcon}
                                        source={require("../../assets/images/search-bar.png")} />
                                </View>
                                <View style={{flex: 1}}>
                                    <TextInput
                                    style={styles.inputContainer}
                                    onChangeText={(text) => findWords(text)}
                                    autoFocus={true}
                                    autoCorrect={true}
                                    placeholder={t('searchScreen_label')}
                                    clearButtonMode={'while-editing'}/>
                                </View>
                            </View>
                            )}
                        inputContainerStyle={styles.inputContainer}
                        containerStyle={styles.container}
                        keyboardShouldPersistTaps="true"
                        data={filteredWords}
                        defaultValue={selectedValue.def}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({item}) => (
                            <View style={{flex: 1}}>
                            </View>
                        )}
                    />
                </View>
                <View style={styles.resultContainer}>
                    {words.length > 0 ? (
                        <SearchedWordResult words={filteredWords} navigation={navigation}></SearchedWordResult>
                    ) : (
                        <Text style={styles.searchText}>
                            Aucun résultat
                        </Text>
                    )}
                </View>
            </View>
        </SafeAreaView>
    );
}

export default SearchScreen;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
    },
    autoCompleteContainer: {
        flex: 1,
        paddingTop: 24,
    },
    searchBarIcon: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 24,
        height: 24,
        resizeMode: 'stretch',
    },
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputContainer: {
        height: 56,
        paddingHorizontal: 5,
        borderRadius : 100,
        backgroundColor: 'white',
        borderWidth: 0
    },
    container: {
        flex: 1,
        paddingHorizontal: 24,
    },
    resultContainer: {
        flex: 13,
    },
    searchText: {
        textAlign: 'center',
        fontSize: 16,
        color: Colors.blackColor,
    }
});
