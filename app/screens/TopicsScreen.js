import React from 'react';
import { StyleSheet, View } from 'react-native';
import TopicList from "./TopicList";

const TopicsScreen = ({navigation}) => {

    return (
        <View style={styles.container}>
            <TopicList navigation={navigation}/>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 24,
    }
});

export default TopicsScreen;