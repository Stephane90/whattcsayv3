import React, { useState, useEffect } from 'react';
import databaseService from "../services/databaseService";
import WordsFromSubtopic from "../components/WordsFromSubtopic";
import transactionService from "../services/transactionService";
import { LocalizationContext } from "../../LocalizationContext";

const WordScreen = ({ props, navigation, route }) => {

    let [rows, setRows] = useState([]);
    let { locale } = React.useContext(LocalizationContext);

    useEffect(() => {
        let subTopicId = route.params != undefined ? route.params.subtopicId : undefined;
        if(subTopicId == undefined) {
            subTopicId = Math.floor(Math.random() * databaseService.getMaxSubTopicId()) + 1
        }
        transactionService.getWords(subTopicId, locale,results => setRows(results));
    }, [locale]);

    return (
        <WordsFromSubtopic words={rows}></WordsFromSubtopic>
    );
}

export default WordScreen;