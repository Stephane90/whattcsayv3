import * as FileSystem from "expo-file-system";
import * as SQLite from "expo-sqlite";
import labelService from "./labelService";
import { Asset } from 'expo-asset';
import {checkAndCreateFolder} from "./checkAndCreateFolder";

const maxSubTopicId = 149;
const databaseName = 'db-2021-11-12.sqlite';
const sqlFolder = `${FileSystem.documentDirectory}SQLite`;
const dbUri = `${FileSystem.documentDirectory}SQLite/`+ databaseName;

class DatabaseService {

    async copyDatabase() {

        await checkAndCreateFolder(sqlFolder);
        console.debug('file system document directory', `${FileSystem.documentDirectory}`);

        const dbInfo = FileSystem.getInfoAsync(dbUri).then(tmp => {
            console.debug('tmp exist :' + tmp.exists);
            console.debug('tmp size :' + tmp.size);
        });

        if(!dbInfo.exists) {
            const copyDatabase = await FileSystem.downloadAsync(
                Asset.fromModule(require('../../assets/www/db-2021-11-12.sqlite')).uri,
                dbUri
            );
            console.debug('copyDatabase', copyDatabase);
        }
    }

    getMaxSubTopicId() {
        return maxSubTopicId;
    }

    getDatabase(){
        const myDB = SQLite.openDatabase(databaseName);
        return myDB;
    }

    getTopicsQuery(locale) {
        const topicColumn = labelService.getTopicColumn(locale);
        return 'SELECT id, '+ topicColumn + ' as text FROM topic where id <> 1 order by id';
    }

    getSubTopicsQuery(id, locale) {
        const subtopicColumn = labelService.getSubtopicColumn(locale);
        const query = 'SELECT id, topic_id, ' + subtopicColumn + ' as text FROM subtopic where topic_id = ' + id + ' order by id';
        //console.debug(query);
        return query;
    }

    getWordsQuery(id, locale){
        const definitionColumn = labelService.getDefinitionColumn(locale);
        const subtopicColumn = labelService.getSubtopicColumn(locale);
        const sqlQuery = "select d.id as definition_id, " + definitionColumn + " as def, p.chinese_char, p.romanization, p.sound_files, s." + subtopicColumn + " as subtopic, " +
            "origin, dialect, part_of_speech, register " +
            "from definition d inner join pengim p " +
            "on d.id = p.definition_id " +
            "inner join subtopic s on d.subtopic_id = s.id " +
            "where subtopic_id = " + id + " order by d.`order` asc";
        //console.debug(sqlQuery);
        return sqlQuery
    }

    getAutocompleteQuery(locale){
        const definitionColumn = labelService.getDefinitionColumn(locale);
        const sqlQuery = "select distinct(p.definition_id), " +
            definitionColumn + " as def, chinese_char, " +
            "origin, dialect, part_of_speech, register " +
            "romanization " +
            "from definition " +
            "inner join pengim p " +
            "on definition.id = p.definition_id";
        //console.debug(sqlQuery);
        return sqlQuery;
    }

    getLikeQuery(likeCriteria, locale){
        const definitionColumn = labelService.getDefinitionColumn(locale);
        const subtopicColumn = labelService.getSubtopicColumn(locale);
        const sqlQuery = "select distinct(p.definition_id), " +
            definitionColumn + " as def, chinese_char, p.sound_files, subtopic_id, " + subtopicColumn + " as subtopic, " +
            "romanization, subtopic.topic_id as topic_id, " +
            "origin, dialect, part_of_speech, register " +
            "from definition " +
            "inner join pengim p " +
            "on definition.id = p.definition_id " +
            "inner join subtopic " +
            "on subtopic_id = subtopic.id " +
            "where " +
            "lower(def) GLOB '*" + this.getGlobVersion(likeCriteria) + "*' or " +
            "lower(accented_romanization) GLOB '*" + this.getGlobVersion(likeCriteria).replace(" ", "") + "*' or " +
            "replace(\n" +
            "       replace(\n" +
            "       replace(\n" +
            "        replace(\n" +
            "        replace(\n" +
            "            chinese_char, ', ', '')\n" +
            "            , ' ', '')\n" +
            "            , '，', '')\n" +
            "    ,'...', '')\n" +
            "     ,'?', '') like '%" + likeCriteria + "%' order by LENGTH(def) asc, def asc"
        //console.debug(sqlQuery);
        return sqlQuery;
    }

    getGlobVersion(likeCriteria) {
        if(likeCriteria != undefined) {
            return likeCriteria.toLowerCase()
                .replace(/[aáàäâãạā]/g, '[aáàäâãạā]')
                .replace(/[eéèëêẹē]/g, '[eéèëêẹē]')
                .replace(/[iíìîịī]/g, '[iíìîịī]')
                .replace(/[oóòöôõọō]/g, '[oóòöôõọō]')
                .replace(/[uúùüûū]/g, '[uúùüûū]')
                .replace(/[cç]/g, '[cç]')
                .replace(/[mṃ]/g, '[mṃ]')
                .replace('*', '[*]')
                .replace('?', '[?]');
        } else {
            return '';
        }
    }
}

const databaseService = new DatabaseService();
export default databaseService;
